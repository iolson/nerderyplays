<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';

    /**
     * Hidden attributes from Eloquent Response
     *
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    /**
     * Game
     *
     * Example:
     *
     * $game = Comment::find(1)->game;
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function game()
    {
        return $this->belongsTo('App\Game');
    }
}