<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'games';

    /**
     * Hidden attributes from Eloquent Response
     *
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    /**
     * Likes
     *
     * Example:
     *
     * $likes = Game::find(1)->likes;
     *
     * foreach ($likes as $like) {
     *      var_dump($like);
     * }
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    /**
     * Comments
     *
     * Example:
     *
     * $comments = Game::find(1)->comments;
     *
     * foreach ($comments as $comment) {
     *      var_dump($comment);
     * }
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * Game Type
     *
     * Example:
     *
     * $gameType = Game::find(1)->types;
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function types()
    {
        return $this->belongsToMany('App\GameType', 'game_type_game');
    }
}