<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * Hidden attributes from Eloquent Response
     *
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
