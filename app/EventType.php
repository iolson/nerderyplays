<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'event_type_event';

    /**
     * Hidden attributes from Eloquent Response
     *
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    /**
     * Get all events that belong to that event type.
     *
     * Example:
     *
     * $events = EventType::find(1)->events;
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function events()
    {
        return $this->belongsToMany('App/Event', 'event_type_event');
    }
}
