<?php

use App\Game;
use App\User;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

/**
 * Testing Route
 */
Route::get('/test', function () {

    $object = Game::find(1);
    $object->types;

    $response = $object;

    return response()->json($response);
});

/**
 * Version 1 of the API
 *
 * @author Ian Olson <ian.olson@nerdery.com>
 */
Route::group(['prefix' => 'v1'], function () {

    // User CRUD
    Route::resource('users', 'UserController');

    // User Role CRUD
    Route::resource('roles', 'UserRoleController');

    // Game CRUD
    Route::resource('games', 'GameController');

    // Game Type CRUD
    Route::resource('game-types', 'GameTypeController');

    // Comment CRUD
    Route::resource('comments', 'CommentController');

    // Like CRUD
    Route::resource('likes', 'LikeController');

    // Event CRUD
    Route::resource('events', 'EventController');

    // Event Type CRUD
    Route::resource('event-types', 'EventTypeController');
});