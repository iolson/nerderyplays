<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GameType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'game_types';

    /**
     * Hidden attributes from Eloquent Response
     *
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    public function games()
    {
        return $this->belongsToMany('App/Game');
    }
}
