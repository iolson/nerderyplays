<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'likes';

    /**
     * Hidden attributes from Eloquent Response
     *
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    public function game()
    {
        return $this->belongsTo('App\Game');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
