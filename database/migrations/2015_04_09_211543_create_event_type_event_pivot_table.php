<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTypeEventPivotTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_type_event', function (Blueprint $table) {
            $table->integer('event_type_id')
                  ->unsigned()
                  ->index();
            $table->foreign('event_type_id')
                  ->references('id')
                  ->on('event_types')
                  ->onDelete('cascade');
            $table->integer('event_id')
                  ->unsigned()
                  ->index();
            $table->foreign('event_id')
                  ->references('id')
                  ->on('events')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_type_event');
    }

}
