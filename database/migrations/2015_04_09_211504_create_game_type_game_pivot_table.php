<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameTypeGamePivotTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_type_game', function (Blueprint $table) {
            $table->integer('game_type_id')
                  ->unsigned()
                  ->index();
            $table->foreign('game_type_id')
                  ->references('id')
                  ->on('game_types')
                  ->onDelete('cascade');
            $table->integer('game_id')
                  ->unsigned()
                  ->index();
            $table->foreign('game_id')
                  ->references('id')
                  ->on('games')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('game_type_game');
    }

}
