<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('roles')
          ->delete();

        Role::create([
            'title' => 'Administrator',
            'slug'  => 'admin'
        ]);

        Role::create([
            'title' => 'User',
            'slug'  => 'user'
        ]);
    }
}