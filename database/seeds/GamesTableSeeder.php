<?php

use App\Game;
use Illuminate\Database\Seeder;

class GamesTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('games')
          ->delete();

        Game::create([
            'title'       => 'Risk',
            'description' => 'Bacon ipsum dolor amet leberkas kevin spare ribs tail, sirloin boudin bacon bresaola. Prosciutto corned beef swine pancetta t-bone turkey frankfurter kielbasa turducken shank bresaola. Boudin swine strip steak biltong pork belly cupim. Tongue ham hock leberkas beef bresaola pastrami pancetta short ribs strip steak alcatra meatball. Tri-tip ribeye venison, frankfurter bresaola tail tongue spare ribs leberkas corned beef ball tip jowl pancetta fatback sirloin. Pork belly flank porchetta alcatra cow pastrami pancetta.',
            'bgg_id'      => 181,
            'user_id'     => 1,
            'status'      => 'owned'
        ]);

        Game::create([
            'title'       => 'Monopoly',
            'description' => 'Bacon ipsum dolor amet leberkas kevin spare ribs tail, sirloin boudin bacon bresaola. Prosciutto corned beef swine pancetta t-bone turkey frankfurter kielbasa turducken shank bresaola. Boudin swine strip steak biltong pork belly cupim. Tongue ham hock leberkas beef bresaola pastrami pancetta short ribs strip steak alcatra meatball. Tri-tip ribeye venison, frankfurter bresaola tail tongue spare ribs leberkas corned beef ball tip jowl pancetta fatback sirloin. Pork belly flank porchetta alcatra cow pastrami pancetta.',
            'bgg_id'      => 1406,
            'user_id'     => 1,
            'status'      => 'owned'
        ]);
    }
}