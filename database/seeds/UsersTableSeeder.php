<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')
          ->delete();

        User::create([
            'username' => 'iolson',
            'email'    => 'ian.olson@nerdery.com',
            'password' => 'password',
            'status'   => 'active'
        ]);

        foreach (User::all() as $user) {
            DB::table('users')
              ->where('id', $user->id)
              ->update(['password' => Hash::make($user->password)]);
        }
    }
}